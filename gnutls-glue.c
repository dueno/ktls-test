/* SPDX-License-Identifier: MIT */

#include "config.h"

#include "gnutls-glue.h"

#include <errno.h>
#include <linux/tls.h>
#include <stdio.h>
#include <string.h>
#include "utils.h"

#define PRIO "NORMAL:-VERS-ALL:+VERS-TLS1.3:" \
  "-CIPHER-ALL:+AES-256-GCM:" \
  "-GROUP-ALL:+GROUP-SECP256R1:+GROUP-X25519:+GROUP-SECP384R1:+GROUP-SECP521R1:" \
  "%DISABLE_TLS13_COMPAT_MODE"

static int
handshake_secret_func (gnutls_session_t session,
                       gnutls_record_encryption_level_t glevel,
                       const void *secret_read, const void *secret_write,
                       size_t secret_size)
{
  gnutls_datum_t mac_key;
  gnutls_datum_t iv;
  gnutls_datum_t cipher_key;
  unsigned char seq_number[8];
  int ret;

  ret = gnutls_record_get_state (session, 0, &mac_key, &iv, &cipher_key,
				 seq_number);
  if (ret < 0)
    {
      fprintf (stderr, "gnutls_record_get_state: %s\n",
	       gnutls_strerror (ret));
      return ret;
    }

  cipher_key.data = (uint8_t *)secret_write;
  cipher_key.size = secret_size;
  ret = install_ktls_keys (session, true, &mac_key, &iv, &cipher_key,
			   seq_number);
  if (ret < 0)
    {
      fprintf (stderr, "install_ktls_keys for egress failed\n");
      return ret;
    }

  ret = gnutls_record_get_state (session, 1, &mac_key, &iv, &cipher_key,
				 seq_number);
  if (ret < 0)
    {
      fprintf (stderr, "gnutls_record_get_state: %s\n",
	       gnutls_strerror (ret));
      return ret;
    }

  cipher_key.data = (uint8_t *)secret_read;
  cipher_key.size = secret_size;
  ret = install_ktls_keys (session, false, &mac_key, &iv, &cipher_key,
			   seq_number);
  if (ret < 0)
    {
      fprintf (stderr, "install_ktls_keys for ingress failed: %s\n",
	       gnutls_strerror (ret));
      return ret;
    }

  return 0;
}

static int
handshake_read_func (gnutls_session_t session,
                     gnutls_record_encryption_level_t glevel,
                     gnutls_handshake_description_t htype,
                     const void *data, size_t data_size)
{
  struct iovec iov[2];
  uint8_t header[4];

  header[0] = htype;
  header[1] = (data_size >> 16) & 0xFF;
  header[2] = (data_size >> 8) & 0xFF;
  header[3] = data_size & 0xFF;

  iov[0].iov_base = header;
  iov[0].iov_len = sizeof(header);

  iov[1].iov_base = (void *)data;
  iov[1].iov_len = data_size;

  return send_ktls_control_msg (session, 22, iov, 2);
}

static int
alert_read_func (gnutls_session_t session,
                 gnutls_record_encryption_level_t level,
                 gnutls_alert_level_t alert_level,
                 gnutls_alert_description_t alert_desc)
{
  struct iovec iov;
  uint8_t payload[2];

  payload[0] = alert_level;
  payload[1] = alert_desc;

  iov.iov_base = payload;
  iov.iov_len = sizeof(payload);

  return send_ktls_control_msg (session, 21, &iov, 1);
}

int
setup_post_handshake (gnutls_session_t session)
{
  gnutls_handshake_set_secret_function (session, handshake_secret_func);
  gnutls_handshake_set_read_function (session, handshake_read_func);
  gnutls_alert_set_read_function (session, alert_read_func);

  return 0;
}

gnutls_certificate_credentials_t
create_tls_client_credentials (const char *ca_file)
{
  __attribute__((cleanup(gnutls_certificate_free_credentialsp)))
    gnutls_certificate_credentials_t cred = NULL;
  int ret;

  ret = gnutls_certificate_allocate_credentials (&cred);
  if (ret < 0)
    {
      fprintf (stderr, "gnutls_certificate_allocate_credentials: %s\n",
	       gnutls_strerror (ret));
      return NULL;
    }

  ret = gnutls_certificate_set_x509_trust_file (cred,
                                                ca_file,
                                                GNUTLS_X509_FMT_PEM);
  if (ret < 0)
    {
      fprintf (stderr, "gnutls_certificate_set_x509_system_trust: %s\n",
	       gnutls_strerror (ret));
      return NULL;
    }

  return steal_pointer (&cred);
}

gnutls_session_t
create_tls_client_session (gnutls_certificate_credentials_t cred)
{
  __attribute__((cleanup(gnutls_deinitp)))
    gnutls_session_t session = NULL;
  int ret;

  ret = gnutls_init (&session,
                     GNUTLS_CLIENT |
                     GNUTLS_ENABLE_EARLY_DATA |
                     GNUTLS_NO_END_OF_EARLY_DATA);
  if (ret < 0)
    {
      fprintf (stderr, "gnutls_init: %s\n",
	       gnutls_strerror (ret));
      return NULL;
    }

  ret = gnutls_priority_set_direct (session, PRIO, NULL);
  if (ret < 0)
    {
      fprintf (stderr, "gnutls_priority_set_direct: %s\n",
	       gnutls_strerror (ret));
      return NULL;
    }

  ret = gnutls_credentials_set (session,
                                GNUTLS_CRD_CERTIFICATE,
                                cred);
  if (ret < 0)
    {
      fprintf (stderr, "gnutls_credentials_set: %s\n",
	       gnutls_strerror (ret));
      return NULL;
    }

  return steal_pointer (&session);
}

gnutls_certificate_credentials_t
create_tls_server_credentials (const char *key_file, const char *cert_file)
{
  __attribute__((cleanup(gnutls_certificate_free_credentialsp)))
    gnutls_certificate_credentials_t cred = NULL;
  int ret;

  ret = gnutls_certificate_allocate_credentials (&cred);
  if (ret < 0)
    {
      fprintf (stderr, "gnutls_certificate_allocate_credentials: %s\n",
	       gnutls_strerror (ret));
      return NULL;
    }

  ret = gnutls_certificate_set_x509_key_file (cred, cert_file, key_file,
                                              GNUTLS_X509_FMT_PEM);
  if (ret < 0)
    {
      fprintf (stderr, "gnutls_certificate_set_x509_system_trust: %s\n",
	       gnutls_strerror (ret));
      return NULL;
    }

  return steal_pointer (&cred);
}

gnutls_session_t
create_tls_server_session (gnutls_certificate_credentials_t cred)
{
  __attribute__((cleanup(gnutls_deinitp)))
    gnutls_session_t session = NULL;
  int ret;

  ret = gnutls_init (&session,
                     GNUTLS_SERVER |
                     GNUTLS_ENABLE_EARLY_DATA |
                     GNUTLS_NO_END_OF_EARLY_DATA);
  if (ret < 0)
    {
      fprintf (stderr, "gnutls_init: %s\n",
	       gnutls_strerror (ret));
      return NULL;
    }

  ret = gnutls_priority_set_direct (session, PRIO, NULL);
  if (ret < 0)
    {
      fprintf (stderr, "gnutls_priority_set_direct: %s\n",
	       gnutls_strerror (ret));
      return NULL;
    }

  ret = gnutls_credentials_set (session,
                                GNUTLS_CRD_CERTIFICATE,
                                cred);
  if (ret < 0)
    {
      fprintf (stderr, "gnutls_credentials_set: %s\n",
	       gnutls_strerror (ret));
      return NULL;
    }

  return steal_pointer (&session);
}
