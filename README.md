# KTLS server/client example using GnuTLS without KTLS support (WIP)

This contains example TLS server and client implementation that uses
Linux kernel TLS (KTLS) as transport.  This does not rely on KTLS
support in GnuTLS, but uses the following functions/callbacks:

- `gnutls_record_get_state` to obtain the internal key state
- `gnutls_handshake_set_secret_function` to get notified on rekey events
- `gnutls_handshake_write` to handle post-handshake messages
- `gnutls_handshake_set_read_function` to send post-handshake messages
- `gnutls_alert_set_read_function` to send Alert messages

## Compiling

1. Install Meson, GnuTLS 3.7.2, and the dependencies

```console
$ dnf install gcc gnutls-devel meson ninja-build
```

2. `meson _build`

3. `meson compile -C _build`

## Running

### Server

```console
$ _build/serv localhost 5556 credentials/server-key.pem credentials/server.pem
```

### Client

```console
$ _build/cli localhost 5556 credentials/ca.pem
```

### Rekeying

TODO

## License

The MIT License
