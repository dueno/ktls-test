struct control_message
{
  unsigned char record_type;
  struct iovec *iov;
  size_t iovcnt;
};

struct connection
{
  struct control_message *pending_messages;
  size_t pending_messages_count;
};

