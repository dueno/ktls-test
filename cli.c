/* SPDX-License-Identifier: MIT */

#include "config.h"

#include "gnutls-glue.h"
#include "utils.h"

#include <assert.h>
#include <errno.h>
#include <error.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/epoll.h>
#include <sys/socket.h>
#include <netinet/tcp.h>
#include <linux/tls.h>
#include <gnutls/gnutls.h>
#include <gnutls/x509.h>

#define MAX_EVENTS 64

static int
run (gnutls_session_t session)
{
  __attribute__((cleanup(closep))) int epoll_fd = -1;
  uint8_t inbuf[MAX_BUF];
  uint8_t outbuf[MAX_BUF];

  epoll_fd = epoll_create1 (0);
  if (epoll_fd < 0)
    {
      fprintf (stderr, "epoll_create1: %m\n");
      return -1;
    }

  int flags;

  flags = fcntl (STDIN_FILENO, F_GETFL, 0);
  if (flags < 0)
    {
      fprintf (stderr, "fcntl: %m\n");
      return -1;
    }
  flags = fcntl (STDIN_FILENO, F_SETFL, flags | O_NONBLOCK);
  if (flags < 0)
    {
      fprintf (stderr, "fcntl: %m\n");
      return -1;
    }

  struct epoll_event ev;

  ev.events = EPOLLIN | EPOLLET;
  ev.data.fd = STDIN_FILENO;
  if (epoll_ctl (epoll_fd, EPOLL_CTL_ADD, ev.data.fd, &ev) < 0)
    {
      fprintf (stderr, "epoll_ctl: %m\n");
      return -1;
    }

  int sockin, sockout;
  gnutls_transport_get_int2 (session, &sockin, &sockout);

  ev.events = EPOLLIN | EPOLLET;
  ev.data.fd = sockin;
  if (epoll_ctl (epoll_fd, EPOLL_CTL_ADD, ev.data.fd, &ev) < 0)
    {
      fprintf (stderr, "epoll_ctl: %m\n");
      return -1;
    }

  ev.events = EPOLLOUT | EPOLLET;
  ev.data.fd = sockout;
  if (epoll_ctl (epoll_fd, EPOLL_CTL_ADD, ev.data.fd, &ev) < 0)
    {
      fprintf (stderr, "epoll_ctl: %m\n");
      return -1;
    }

  for (;;)
    {
      struct epoll_event events[MAX_EVENTS];
      int nfds;

      nfds = epoll_wait (epoll_fd, events, MAX_EVENTS, -1);
      if (nfds < 0)
        {
          fprintf (stderr, "epoll_wait: %m\n");
          return -1;
        }

      for (int n = 0; n < nfds; n++)
        {
	  int ret;

          if (events[n].data.fd == sockin)
            {
              if (events[n].events & EPOLLIN)
                {
		  unsigned char record_type;
		  struct iovec iov;
		  iov.iov_base = inbuf;
		  iov.iov_len = sizeof(inbuf);

		  ret = recv_ktls_control_msg (session, &record_type,
					       &iov, 1);
                  if (ret < 0)
                    return -1;
		  if (ret == 0)
		    return 0;

		  switch (record_type)
		    {
		    case 23:	/* application_data */
		      ret = writev (STDOUT_FILENO, &iov, 1);
		      if (ret < 0)
			return -1;
		      break;
		    case 22:	/* handshake */
		      ret = gnutls_handshake_write (session,
						    GNUTLS_ENCRYPTION_LEVEL_APPLICATION,
						    iov.iov_base, ret);
		      if (ret < 0)
			return -1;
		      break;
		    default:
		      return -1;
		    }
                }
            }

          if (events[n].data.fd == sockout)
            {
              if (events[n].events & EPOLLOUT)
                {
		  unsigned char record_type;
		  struct iovec iov;
		  uint8_t buffer[MAX_BUF];
		  iov.iov_base = buffer;
		  iov.iov_len = sizeof(buffer);

		  ret = send_ktls_control_msg (session, 23, /* application_data */
					       &iov, 1);
                  if (ret < 0)
                    return -1;
                }
	    }

          if (events[n].data.fd == STDIN_FILENO)
            {
              ret = handle_stdin (client);
              if (ret < 0)
                return -1;
              if (connection_is_closed (client->connection))
                {
                  close (epoll_fd);
                  return 0;
                }
            }
        }
    }

  return 0;
}

int
main(int argc, char **argv)
{
  /* Create a client socket */
  struct sockaddr_storage local_addr, remote_addr;
  size_t local_addrlen = sizeof(local_addr), remote_addrlen;

  if (argc != 4)
    {
      fprintf (stderr, "Usage: cli HOST PORT CA\n");
      return EXIT_FAILURE;
    }

  __attribute__((cleanup(closep))) int fd = -1;

  fd = resolve_and_connect (argv[1], argv[2],
                            (struct sockaddr *)&local_addr,
                            &local_addrlen,
                            (struct sockaddr *)&remote_addr,
                            &remote_addrlen);
  if (fd < 0)
    error (EXIT_FAILURE, errno, "resolve_and_connect failed\n");

  if (setsockopt (fd, SOL_TCP, TCP_ULP, "tls", sizeof("tls")))
    error (EXIT_FAILURE, errno, "setsockopt failed\n");

  /* Create a TLS client session */
  __attribute__((cleanup(gnutls_certificate_free_credentialsp)))
    gnutls_certificate_credentials_t cred = NULL;

  cred = create_tls_client_credentials (argv[3]);
  if (!cred)
    error (EXIT_FAILURE, EINVAL, "create_tls_client_credentials failed\n");

  __attribute__((cleanup(gnutls_deinitp))) gnutls_session_t session = NULL;

  session = create_tls_client_session (cred);
  if (!session)
    error (EXIT_FAILURE, EINVAL, "create_tls_client_session failed\n");

  gnutls_session_set_verify_cert (session, argv[1], 0);

  gnutls_transport_set_int (session, fd);
  gnutls_handshake_set_timeout (session, GNUTLS_DEFAULT_HANDSHAKE_TIMEOUT);

  int ret;
  do
    {
      ret = gnutls_handshake (session);
    }
  while (ret < 0 && !gnutls_error_is_fatal (ret));

  if (ret < 0)
    error (EXIT_FAILURE, EINVAL, "handshake failed: %s\n",
	   gnutls_strerror (ret));

  char *desc = gnutls_session_get_desc (session);
  printf ("- Session info: %s\n", desc);
  gnutls_free (desc);

  gnutls_datum_t mac_key;
  gnutls_datum_t iv;
  gnutls_datum_t cipher_key;
  unsigned char seq_number[8];

  ret = gnutls_record_get_state (session, 0, &mac_key, &iv, &cipher_key,
				 seq_number);
  if (ret < 0)
    error (EXIT_FAILURE, EINVAL, "gnutls_record_get_state: %s\n",
	   gnutls_strerror (ret));

  ret = install_ktls_keys (session, true, &mac_key, &iv, &cipher_key,
			   seq_number);
  if (ret < 0)
    error (EXIT_FAILURE, EINVAL, "install_ktls_keys for egress\n");

  ret = gnutls_record_get_state (session, 1, &mac_key, &iv, &cipher_key,
				 seq_number);
  if (ret < 0)
    error (EXIT_FAILURE, EINVAL, "gnutls_record_get_state: %s\n",
	   gnutls_strerror (ret));

  ret = install_ktls_keys (session, false, &mac_key, &iv, &cipher_key,
			   seq_number);
  if (ret < 0)
    error (EXIT_FAILURE, EINVAL, "install_ktls_keys for ingress\n");

  setup_post_handshake (session);

  return 0;
}

