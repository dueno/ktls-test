/* SPDX-License-Identifier: MIT */

#include "config.h"

#include "utils.h"

#include <errno.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

int
resolve_and_connect (const char *host, const char *port,
                     struct sockaddr *local_addr, size_t *local_addrlen,
                     struct sockaddr *remote_addr, size_t *remote_addrlen)
{
  struct addrinfo hints;
  struct addrinfo *result, *rp;
  int ret, fd;

  memset (&hints, 0, sizeof(hints));
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;

  ret = getaddrinfo (host, port, &hints, &result);
  if (ret != 0)
    return -1;

  for (rp = result; rp != NULL; rp = rp->ai_next)
    {
      fd = socket (rp->ai_family, rp->ai_socktype | SOCK_NONBLOCK,
                   rp->ai_protocol);
      if (fd == -1)
        continue;

      if (connect (fd, rp->ai_addr, rp->ai_addrlen) == 0)
        {
          *remote_addrlen = rp->ai_addrlen;
          memcpy(remote_addr, rp->ai_addr, rp->ai_addrlen);

          socklen_t len = (socklen_t)*local_addrlen;
          if (getsockname (fd, local_addr, &len) == -1)
            return -1;
          *local_addrlen = len;
          break;
        }

      close (fd);
    }

  freeaddrinfo (result);

  if (rp == NULL)
    return -1;

  return fd;
}

int
resolve_and_bind (const char *host, const char *port,
                  struct sockaddr *local_addr, size_t *local_addrlen)
{
  struct addrinfo hints;
  struct addrinfo *result, *rp;
  int ret, fd;

  memset (&hints, 0, sizeof(hints));
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags  =  AI_PASSIVE;

  ret = getaddrinfo (host, port, &hints, &result);
  if (ret != 0)
    return -1;

  for (rp = result; rp != NULL; rp = rp->ai_next)
    {
      fd = socket (rp->ai_family, rp->ai_socktype, rp->ai_protocol);
      if (fd == -1)
        continue;

      if (bind (fd, rp->ai_addr, rp->ai_addrlen) == 0)
        {
          *local_addrlen = rp->ai_addrlen;
          memcpy(local_addr, rp->ai_addr, rp->ai_addrlen);
          break;
        }

      close (fd);
    }

  freeaddrinfo(result);

  if (rp == NULL)
    return -1;

  return fd;
}

int
install_ktls_keys (int fd, bool egress,
		   const uint8_t *iv,
		   const uint8_t *key,
		   unsigned char seq_number[8])
{
  struct tls12_crypto_info_aes_gcm_256 crypto_info;
  crypto_info.info.version = TLS_1_3_VERSION;
  crypto_info.info.cipher_type = TLS_CIPHER_AES_GCM_256;
  memcpy (crypto_info.iv, iv + 4,
	  TLS_CIPHER_AES_GCM_256_IV_SIZE);
  memcpy (crypto_info.rec_seq, seq_number,
	  TLS_CIPHER_AES_GCM_256_REC_SEQ_SIZE);
  memcpy (crypto_info.key, key,
	  TLS_CIPHER_AES_GCM_256_KEY_SIZE);
  memcpy (crypto_info.salt, iv,
	  TLS_CIPHER_AES_GCM_256_SALT_SIZE);

  if (setsockopt (fd, SOL_TLS, egress ? TLS_TX : TLS_RX,
		  &crypto_info, sizeof(crypto_info)))
    {
      fprintf (stderr, "setsockopt failed\n");
      return -errno;
    }

  return 0;
}

int
send_ktls_control_msg (int fd,
		       unsigned char record_type,
		       struct iovec *iov, size_t iovcnt)
{
  ssize_t ret;

  char cmsg[CMSG_SPACE(sizeof (unsigned char))];
  struct msghdr msg = { 0 };
  struct cmsghdr *hdr;

  msg.msg_control = cmsg;
  msg.msg_controllen = sizeof cmsg;

  hdr = CMSG_FIRSTHDR(&msg);
  hdr->cmsg_level = SOL_TLS;
  hdr->cmsg_type = TLS_SET_RECORD_TYPE;
  hdr->cmsg_len = CMSG_LEN(sizeof (unsigned char));

  *CMSG_DATA(hdr) = record_type;
  msg.msg_controllen = hdr->cmsg_len;

  msg.msg_iov = iov;
  msg.msg_iovlen = iovcnt;

  return sendmsg (fd, &msg, MSG_DONTWAIT);
}

int
recv_ktls_control_msg (int fd,
		       unsigned char *record_type,
		       struct iovec *iov, size_t iovcnt)
{
  ssize_t ret;

  char cmsg[CMSG_SPACE(sizeof (unsigned char))];
  struct msghdr msg = { 0 };
  struct cmsghdr *hdr;

  msg.msg_control = cmsg;
  msg.msg_controllen = sizeof cmsg;

  msg.msg_iov = iov;
  msg.msg_iovlen = iovcnt;

  ret = recvmsg (fd, &msg, MSG_DONTWAIT);
  if (ret < 0)
    return 0;

  if (ret > 0)
    {
      /* get record type from header */
      hdr = CMSG_FIRSTHDR(&msg);
      if (hdr == NULL)
	return -1;

      if (hdr->cmsg_level == SOL_TLS && hdr->cmsg_type == TLS_GET_RECORD_TYPE)
	*record_type = *(unsigned char *)CMSG_DATA(hdr);
      else
	*record_type = 23;		/* application_data */
    }

  return ret;
}

