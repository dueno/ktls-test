/* SPDX-License-Identifier: MIT */

#ifndef UTILS_H_
#define UTILS_H_

#include <stdint.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

int resolve_and_connect (const char *host, const char *port,
                         struct sockaddr *local_addr, size_t *local_addrlen,
                         struct sockaddr *remote_addr, size_t *remote_addrlen);

int resolve_and_bind (const char *host, const char *port,
                      struct sockaddr *local_addr, size_t *local_addrlen);

int install_ktls_keys (int fd, bool egress,
		       const uint8_t *iv,
		       const uint8_t *key,
		       unsigned char seq_number[8]);
int send_ktls_control_messsage (int fd,
				unsigned char record_type,
				struct iovec *iov, size_t iovcnt);
int receive_ktls_control_message (int fd,
				  unsigned char *record_type,
				  struct iovec *iov, size_t iovcnt);

/* For __attribute__((cleanup)) */
static inline void
closep (int *p)
{
  int fd = *p;
  if (fd >= 0)
    close (fd);
}

static inline int
steal_fd (int *p)
{
  int fd = *p;
  *p = -1;
  return fd;
}

static inline void *
steal_pointer (void *p)
{
  void *pp = *(void **)p;
  *(void **)p = NULL;
  return pp;
}

#endif  /* UTILS_H_ */
