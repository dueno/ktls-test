struct control_message
{
  unsigned char record_type;
  struct iovec *iov;
  size_t iovcnt;
};

struct connection
{
  struct control_message *pending;
  size_t pending_length;
  size_t pending_capacity;
};

int
connection_push_message (struct connection *connection,
			 unsigned char record_type,
			 struct iovec *iov,
			 size_t iovcnt)
{
  if (1 < connection->pending_capacity - connection->pending_length)
    {
      struct control_message *new_pending;
      size_t new_capacity = (connection->pending_capacity + 1) * 2;

      new_pending = reallocarray (connection->pending, new_capacity,
				  sizeof(struct control_message));
      if (!new_pending)
	return -ENOMEM;

      connection->pending = new_pending;
      connection->pending_capacity = new_capacity;
    }

  for (size_t i = 0; i < iovcnt; i++)
    {
      
    }
}
